package biblioteka;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Biblioteka {

    List<Ksiazki> ksiazki = new ArrayList<>();
    List<Magazyny> magazyny = new ArrayList<>();
    List<Uzytkownicy> uzytkownicy = new ArrayList<>();
    List<Pozyczone> pozyczone = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        Biblioteka plik = Biblioteka.getInstance();
        Scanner in = new Scanner(System.in);
        String komenda = "";

        plik.czytajPlik("ksiazki");
        plik.czytajPlik("magazyny");
        plik.czytajPlik("uzytkownicy");
        plik.czytajPlik("pozyczone");

        System.out.println("Komendy:");
        System.out.println("Dodaj Użytkownicy");
        System.out.println("Wyświetl Użytkownicy");
        System.out.println("Dodaj Książki");
        System.out.println("Wyświetl Książki");
        System.out.println("Dodaj Magazyny");
        System.out.println("Wyświetl Magazyny");
        System.out.println("Wypożycz");
        System.out.println("Wygeneruj Klientów");
        System.out.println("Wczytaj Książki z pliku");
        System.out.println("Exit");
        System.out.println();

        while (!komenda.equals("Exit")) {
            System.out.println("Program biblioteka 2018");
            System.out.println();
            komenda = in.nextLine();

            switch (komenda) {
                case "Dodaj Użytkownicy":
                    System.out.println("Imię");
                    String imie = in.nextLine();
                    System.out.println("Podaj Nazwisko");
                    String nazwisko = in.nextLine();
                    System.out.println("Podaj Numer karty");
                    String numerKarty = in.nextLine();
                    System.out.println("(W)ykladowca czy (S)tudent?");
                    String typ = in.nextLine();
                    if ((typ.equals("W")) || (typ.equals("S"))) {
                        plik.dodajUzytkownicy(imie, nazwisko, numerKarty, typ);
                    } else {
                        System.out.println("Niedozwolona wartość");
                    }

                    break;
                case "Wyświetl Użytkownicy":
                    plik.wyswietlUzytkownicy();
                    break;
                case "Dodaj Książki":
                    System.out.println("Podaj Tytuł");
                    String tytulKsiazki = in.nextLine();
                    System.out.println("Podaj Autora");
                    String autorKsiazki = in.nextLine();
                    System.out.println("Podaj Ilość");
                    String iloscKsiazek = in.nextLine();

                    plik.dodajKsiazke(tytulKsiazki, autorKsiazki, iloscKsiazek);
                    break;
                case "Wyświetl Książki":

                    plik.wyswietlKsiazki();
                    break;
                case "Dodaj Magazyny":
                    System.out.println("Podaj Tytuł");
                    String tytulMagazynu = in.nextLine();
                    System.out.println("Podaj Numer");
                    String numerMagazynu = in.nextLine();
                    System.out.println("Podaj Ilość");
                    String iloscMagazynow = in.nextLine();

                    plik.dodajMagazyn(tytulMagazynu, numerMagazynu, iloscMagazynow);
                    break;
                case "Wyswietl Magazyny":
                    plik.wyswietlMagazyny();
                    break;
                case "Wypożycz":
                    System.out.println("Podaj Numer karty");
                    String numerKartyUzytkownika = in.nextLine();
                    if (!plik.czyMozeWypozyczyc(numerKartyUzytkownika)) {
                        System.out.println("Wyczerpany limit");
                    } else {
                        System.out.println("Podaj Tytuł");
                        String tytul = in.nextLine();
                        System.out.println("Podaj Autora ksiazki lub numer Magazynu");
                        String autorNumer = in.nextLine();
                        plik.wypozycz(numerKartyUzytkownika, tytul, autorNumer);
                    }
                case "Wygeneruj Klientów":
                    plik.wygenerujKlientow();
                    break;
                case "Wczytaj Książki z pliku":
                    plik.wczytajZPliku();
                    break;
                case "Exit":
                    break;
                default:
                    System.out.println("Nierozpoznana komenda");
            }

        }

        System.out.println();

        System.out.println("Program biblioteka zakończyć działanie");
    }

    private static Biblioteka getInstance() {
        return new Biblioteka();
    }

    private void dodajKsiazke(String tytulKsiazki, String autorKsiazki, String iloscKsiazek) throws IOException {
        boolean czyObecna = false;
        for (Ksiazki ksiazki1 : ksiazki) {
            if ((ksiazki1.tytul.equals(tytulKsiazki)) && (ksiazki1.autor.equals(autorKsiazki))) {
                ksiazki1.ilosc = Integer.toString(Integer.parseInt(ksiazki1.ilosc) + Integer.parseInt(iloscKsiazek));
                czyObecna = true;
            }
        }
        if (!czyObecna) {
            ksiazki.add(new Ksiazki(tytulKsiazki, autorKsiazki, iloscKsiazek));
        }
        zapiszKsiazki();
    }

    private void zapiszKsiazki() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("ksiazki.txt"));
        String zawartosc = "Tytuł;Autor;Ilość\r\n";
        try {

            for (Ksiazki ksiazki1 : ksiazki) {
                zawartosc += (ksiazki1.tytul + ";" + ksiazki1.autor + ";" + ksiazki1.ilosc + "\r\n");
            }

            bw.write(zawartosc);
        } finally {
            bw.close();
        }

    }

    private void dodajMagazyn(String tytul, String numer, String ilosc) throws IOException {
        boolean czyObecny = false;
        for (Magazyny magazyny1 : magazyny) {
            if ((magazyny1.tytul.equals(tytul)) && (magazyny1.numer.equals(numer))) {
                magazyny1.ilosc = Integer.toString(Integer.parseInt(magazyny1.ilosc) + Integer.parseInt(ilosc));
                czyObecny = true;
            }
        }
        if (!czyObecny) {
            magazyny.add(new Magazyny(tytul, numer, ilosc));
        }
        zapiszMagazyny();
    }

    private void zapiszMagazyny() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("magazyny.txt"));
        String zawartosc = "Tytuł;Numer;Ilość wszystkich\r\n";
        try {

            for (Magazyny magazyny1 : magazyny) {
                zawartosc += (magazyny1.tytul + ";" + magazyny1.numer + ";" + magazyny1.ilosc + "\r\n");
            }

            bw.write(zawartosc);
        } finally {
            bw.close();
        }

    }

    private void dodajUzytkownicy(String imie, String nazwisko, String numerkarty, String typ) throws IOException {
        boolean czyObecny = false;
        for (Uzytkownicy uzytkownicy1 : uzytkownicy) {
            if ((uzytkownicy1.numerKarty.equals(numerkarty))) {
                System.out.println("Użytkownik widnieje już w bazie");
                czyObecny = true;
            }
        }
        if (!czyObecny) {
            uzytkownicy.add(new Uzytkownicy(imie, nazwisko, numerkarty, typ));
        }
        zapiszUzytkownicy();
    }

    private void zapiszUzytkownicy() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("uzytkownicy.txt"));
        String zawartosc = "Imię;Nazwisko;Nr karty;Typ\r\n";
        try {

            for (Uzytkownicy uzytkownicy1 : uzytkownicy) {
                zawartosc += (uzytkownicy1.imie + ";" + uzytkownicy1.nazwisko + ";" + uzytkownicy1.numerKarty + ";" + uzytkownicy1.typ + "\r\n");
            }

            bw.write(zawartosc);
        } finally {
            bw.close();
        }

    }

    private String[] czytajPlik(String plik) throws IOException {
        String[] zawartosc;
        BufferedReader br = new BufferedReader(new FileReader(plik + ".txt"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (true) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                if (line == null) {
                    break;
                }
                switch (plik) {
                    case "ksiazki":
                        ksiazki.add(new Ksiazki(line.split(";")[0], line.split(";")[1], line.split(";")[2]));
                        break;
                    case "magazyny":
                        magazyny.add(new Magazyny(line.split(";")[0], line.split(";")[1], line.split(";")[2]));
                        break;
                    case "uzytkownicy":
                        uzytkownicy.add(new Uzytkownicy(line.split(";")[0], line.split(";")[1], line.split(";")[2], line.split(";")[3]));
                        break;
                    case "pozyczone":
                        pozyczone.add(new Pozyczone(line.split(";")[0], line.split(";")[1], line.split(";")[2]));
                        break;
                }
            }
            zawartosc = sb.toString().split("\r\n");
        } finally {
            br.close();
        }
        return zawartosc;
    }

    private void wyswietlKsiazki() {
        for (Ksiazki ksiazki1 : ksiazki) {
            System.out.println(ksiazki1.autor + " " + ksiazki1.tytul + " " + ksiazki1.ilosc);
        }
    }

    private void wyswietlMagazyny() {
        for (Magazyny magazyny1 : magazyny) {
            System.out.println(magazyny1.tytul + " " + magazyny1.numer + " " + magazyny1.ilosc);
        }
    }

    private void wyswietlUzytkownicy() {
        for (Uzytkownicy uzytkownicy1 : uzytkownicy) {
            System.out.println(uzytkownicy1.imie + " " + uzytkownicy1.nazwisko + " " + uzytkownicy1.numerKarty + " " + uzytkownicy1.typ);
        }
    }

    private void wyswietlPozyczone() {
        for (Pozyczone pozyczone1 : pozyczone) {
            System.out.println(pozyczone1.numerKarty + " " + pozyczone1.tytul + " " + pozyczone1.autorNumer);
        }
    }

    private boolean czyMozeWypozyczyc(String numerKarty) throws IOException {
        int ileWolnoPozyczyc = 4;
        int ilePozyczyl = 0;
        String[] lista;

        for (Uzytkownicy uzytkownicy1 : uzytkownicy) {
            if ((uzytkownicy1.numerKarty.equals(numerKarty))) {
                if (uzytkownicy1.typ.equals("W")) {
                    ileWolnoPozyczyc = 10;
                }
            }
        }

        BufferedReader br = new BufferedReader(new FileReader("pozyczone.txt"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (true) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                if (line == null) {
                    break;
                }

                if (line.split(";")[0].equals(numerKarty)) {
                    ilePozyczyl++;

                }
            }
        } finally {
            br.close();
        }

        return ilePozyczyl < ileWolnoPozyczyc;
    }

    private void wypozycz(String numerKarty, String tytul, String autorNumer) throws IOException {
        for (Ksiazki ksiazki1 : ksiazki) {
            if ((ksiazki1.tytul.equals(tytul)) && (ksiazki1.autor.equals(autorNumer))) {
                ksiazki1.ilosc = Integer.toString(Integer.parseInt(ksiazki1.ilosc) - 1);
                zapiszKsiazki();
            }
        }

        for (Magazyny magazyny1 : magazyny) {
            if ((magazyny1.tytul.equals(tytul)) && (magazyny1.numer.equals(autorNumer))) {
                magazyny1.ilosc = Integer.toString(Integer.parseInt(magazyny1.ilosc) - 1);
                zapiszMagazyny();
            }
        }

        pozyczone.add(new Pozyczone(numerKarty, tytul, autorNumer));
        BufferedWriter bw = new BufferedWriter(new FileWriter("pozyczone.txt"));
        String zawartosc = "NumerKarty;Tytul;Autor\\Numer\r\n";
        try {

            for (Pozyczone pozyczone1 : pozyczone) {
                zawartosc += (pozyczone1.numerKarty + ";" + pozyczone1.tytul + ";" + pozyczone1.autorNumer + "\r\n");
            }

            bw.write(zawartosc);
            System.out.println("Wypożyczono");
        } finally {
            bw.close();
        }
    }

    private void wygenerujKlientow() throws IOException {
        String zawartosc = "";
        BufferedWriter bw = new BufferedWriter(new FileWriter("klienci.txt"));
        boolean jestObecny = false;
        try {

            for (Uzytkownicy uzytkownicy1 : uzytkownicy) {
                jestObecny = false;
                for (Pozyczone pozyczone1 : pozyczone) {
                    if (uzytkownicy1.numerKarty.equals(pozyczone1.numerKarty)) {
                        jestObecny = true;
                    }
                }

                if (jestObecny) {
                    zawartosc += uzytkownicy1.imie + " " + uzytkownicy1.nazwisko + " [";
                }
                for (Pozyczone pozyczone1 : pozyczone) {
                    if (uzytkownicy1.numerKarty.equals(pozyczone1.numerKarty)) {
                        zawartosc += pozyczone1.tytul + "-" + pozyczone1.autorNumer + "; ";
                    }
                }
                if (jestObecny) {
                    zawartosc += "]\r\n";
                }
            }
            bw.write(zawartosc);
            System.out.println("Wygenerowano");
        } finally {
            bw.close();
        }
    }

    private void wczytajZPliku() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("plikzksiazkami.txt"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (true) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                if (line == null) {
                    break;
                }

               dodajKsiazke(line.split(";")[0], line.split(";")[1], line.split(";")[2]);
                   

                
            }
        } finally {
            br.close();
        }
    }

}

final class Ksiazki {

    String tytul;
    String autor;
    String ilosc;

    public Ksiazki(String tytul, String autor, String ilosc) {
        this.tytul = tytul;
        this.autor = autor;
        this.ilosc = ilosc;
    }

}

final class Magazyny {

    String tytul;
    String numer;
    String ilosc;

    public Magazyny(String tytul, String numer, String ilosc) {
        this.tytul = tytul;
        this.numer = numer;
        this.ilosc = ilosc;
    }

}

final class Uzytkownicy {

    String imie;
    String nazwisko;
    String numerKarty;
    String typ;

    public Uzytkownicy(String imie, String nazwisko, String numerKarty, String typ) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerKarty = numerKarty;
        this.typ = typ;
    }

}

final class Pozyczone {

    String numerKarty;
    String tytul;
    String autorNumer;

    public Pozyczone(String numerKarty, String tytul, String autorNumer) {
        this.numerKarty = numerKarty;
        this.tytul = tytul;
        this.autorNumer = autorNumer;
    }

}
